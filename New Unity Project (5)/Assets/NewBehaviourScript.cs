﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour
{
    float rotspeed = 0;
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.LeftArrow)) {
            this.rotspeed = -1;
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            this.rotspeed = +1;
        }
        transform.Rotate(0, 0, this.rotspeed);
        this.rotspeed *= 0f;
    }
}
